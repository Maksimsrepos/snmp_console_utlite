﻿using SnmpSharpNet;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ConsoleApp1
{
    public class SuperSimpleSnmp
    {
        public List<Printer> Devices { get; private set; }
        public List<Trap> Traps { get; private set; }
        private TrapAgent agent;
        private Thread listener;

        public SuperSimpleSnmp()
        {
            Devices = new List<Printer>();
            Traps = new List<Trap>();
            agent = new TrapAgent();

            listener = new Thread(() => 
            {
                // Construct a socket and bind it to the trap manager port 162 
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 162);
                EndPoint ep = (EndPoint)ipep;
                socket.Bind(ep);
                // Disable timeout processing. Just block until packet is received 
                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 0);
                bool run = true;
                int inlen = -1;
                while (run)
                {
                    byte[] indata = new byte[16 * 1024];
                    // 16KB receive buffer int inlen = 0;
                    IPEndPoint peer = new IPEndPoint(IPAddress.Any, 0);
                    EndPoint inep = (EndPoint)peer;
                    try
                    {
                        inlen = socket.ReceiveFrom(indata, ref inep);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception {0}", ex.Message);
                        inlen = -1;
                    }
                    if (inlen > 0)
                    {
                        // Check protocol version int 
                        int ver = SnmpPacket.GetProtocolVersion(indata, inlen);
                        if (ver == (int)SnmpVersion.Ver1)
                        {
                            // Parse SNMP Version 1 TRAP packet 
                            SnmpV1TrapPacket pkt = new SnmpV1TrapPacket();
                            pkt.decode(indata, inlen);
                            Console.WriteLine("** SNMP Version 1 TRAP received from {0}:", inep.ToString());
                            Console.WriteLine("*** Trap generic: {0}", pkt.Pdu.Generic);
                            Console.WriteLine("*** Trap specific: {0}", pkt.Pdu.Specific);
                            Console.WriteLine("*** Agent address: {0}", pkt.Pdu.AgentAddress.ToString());
                            Console.WriteLine("*** Timestamp: {0}", pkt.Pdu.TimeStamp.ToString());
                            Console.WriteLine("*** VarBind count: {0}", pkt.Pdu.VbList.Count);
                            Console.WriteLine("*** VarBind content:");
                            foreach (Vb v in pkt.Pdu.VbList)
                            {
                                Console.WriteLine("**** {0} {1}: {2}", v.Oid.ToString(), SnmpConstants.GetTypeName(v.Value.Type), v.Value.ToString());
                            }
                            Console.WriteLine("** End of SNMP Version 1 TRAP data.");
                        }
                        else
                        {
                            // Parse SNMP Version 2 TRAP packet 
                            SnmpV2Packet pkt = new SnmpV2Packet();
                            pkt.decode(indata, inlen);
                            Console.WriteLine("** SNMP Version 2 TRAP received from {0}:", inep.ToString());
                            if ((SnmpSharpNet.PduType)pkt.Pdu.Type != PduType.V2Trap)
                            {
                                Console.WriteLine("*** NOT an SNMPv2 trap ****");
                            }
                            else
                            {
                                Console.WriteLine("*** Community: {0}", pkt.Community.ToString());
                                Console.WriteLine("*** VarBind count: {0}", pkt.Pdu.VbList.Count);
                                Console.WriteLine("*** VarBind content:");
                                foreach (Vb v in pkt.Pdu.VbList)
                                {
                                    Console.WriteLine("**** {0} {1}: {2}",
                                       v.Oid.ToString(), SnmpConstants.GetTypeName(v.Value.Type), v.Value.ToString());
                                }
                                Console.WriteLine("** End of SNMP Version 2 TRAP data.");
                            }
                        }
                    }
                    else
                    {
                        if (inlen == 0)
                            Console.WriteLine("Zero length packet received.");
                    }
                }
            });
            listener.Start();
        }

        public void StartScan(IpAddress fromIp, IpAddress toIp)
        {
            IpAddress ipforscan = fromIp;

            OctetString community = new OctetString("public");

            AgentParameters param = new AgentParameters(community);

            param.Version = SnmpVersion.Ver1;

            Dictionary<IpAddress, string> resultDict = new Dictionary<IpAddress, string>();

            while (ipforscan.CompareTo(toIp) == -1)
            {

                try
                {

                    UdpTarget target = new UdpTarget((System.Net.IPAddress)ipforscan, 161, 2, 2);

                    Pdu pdu = new Pdu(PduType.Get);
                    pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.3.1"); 
                    //pdu.VbList.Add("1.3.6.1.2.1.1.1.0");
                    SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);
                    if (result != null)
                    {
                        var temp = new Printer(target, result.Pdu.VbList[0].Value.ToString());

                        if (!Devices.Contains(temp))
                            Devices.Add(temp);
                        
                    }
                    target.Close();
                }
                catch (Exception exs)
                {
                    Console.WriteLine(exs.Message);
                }


                ipforscan = ipforscan.Increment(1);

            }
        }
        /// <summary>
        /// Опрашивает все mibs устройства. 
        /// </summary>
        /// <param name="adr"></param>
        public void GetInfo(string adr = "10.10.0.32")
        {
            Console.WriteLine();
            // SNMP community name
            OctetString community = new OctetString("public");
            int n = 0;
            // Define agent parameters class
            AgentParameters param = new AgentParameters(community);
            // Set SNMP version to 1
            param.Version = SnmpVersion.Ver1;
            // Construct the agent address object
            // IpAddress class is easy to use here because
            //  it will try to resolve constructor parameter if it doesn't
            //  parse to an IP address
            IpAddress agent = new IpAddress(adr);//10.10.0.28  9

            // Construct target
            UdpTarget target = new UdpTarget((IPAddress)agent, 161, 20, 1);

            // Define Oid that is the root of the MIB
            //  tree you wish to retrieve
            Oid rootOid = new Oid("1"); // ifDescr
            // This Oid represents last Oid returned by
            //  the SNMP agent
            Oid lastOid = (Oid)rootOid.Clone(); //; new Oid("1.3.6.1.4.1.1347.40.10.1.1.5.1")

            // Pdu class used for all requests
            Pdu pdu = new Pdu(PduType.GetNext);

            SnmpV1Packet result = new SnmpV1Packet();
            // Loop through results
            while (lastOid != null && n < 49)
            {

                // When Pdu class is first constructed, RequestId is set to a random value
                // that needs to be incremented on subsequent requests made using the
                // same instance of the Pdu class.
                if (pdu.RequestId != 0)
                {
                    pdu.RequestId += 1;
                }
                // Clear Oids from the Pdu class.
                pdu.VbList.Clear();
                // Initialize request PDU with the last retrieved Oid
                pdu.VbList.Add(lastOid);
                // Make SNMP request
                try
                {
                    result = (SnmpV1Packet)target.Request(pdu, param);
                }
                catch (SnmpException)
                {
                    Console.WriteLine(/*"Exception message " + e.Message +*/
                        "\n End of quene.");

                    break;
                }
                // You should catch exceptions in the Request if using in real application.

                // If result is null then agent didn't reply or we couldn't parse the reply.
                if (result != null)
                {

                    // ErrorStatus other then 0 is an error returned by 
                    // the Agent - see SnmpConstants for error definitions
                    if (result.Pdu.ErrorStatus != 0)
                    {
                        // agent reported an error with the request
                        Console.WriteLine("Error in SNMP reply. Error {0} index {1}",
                            result.Pdu.ErrorStatus,
                            result.Pdu.ErrorIndex);
                        lastOid = null;
                        break;
                    }
                    else
                    {
                        // Walk through returned variable bindings
                        foreach (Vb v in result.Pdu.VbList)
                        {
                            // Check that retrieved Oid is "child" of the root OID
                            if (rootOid.IsRootOf(v.Oid))
                            {
                                Console.WriteLine("{0} ({1}): {2}",
                                    v.Oid.ToString(),
                                    SnmpConstants.GetTypeName(v.Value.Type),
                                    v.Value.ToString());
                                lastOid = v.Oid;
                                n++;
                            }
                            else
                            {
                                // we have reached the end of the requested
                                // MIB tree. Set lastOid to null and exit loop
                                lastOid = null;
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("No response received from SNMP agent.");
                }

                if (n == 49)
                {
                    Console.WriteLine("\nContinue?\n 1 - yes\n any - no\n");
                    if (Console.ReadKey(true) == new ConsoleKeyInfo('1', ConsoleKey.D1, false, false, false))
                    {
                        n = 0;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            target.Close();
        }

        /// <summary>
        /// Выполняет актуализацию данных всех принтеров находящихся в коллекции Devices
        /// Актуализация выполняется путём пересоздания объекта устройства.
        /// </summary>
        public void Rescan()
        {
            foreach (var printer in Devices)
            {
                OctetString community = new OctetString("public");

                AgentParameters param = new AgentParameters(community);

                param.Version = SnmpVersion.Ver1;

                UdpTarget target = new UdpTarget((System.Net.IPAddress)printer.Ip, 161, 2, 2);

                Pdu pdu = new Pdu(PduType.Get);
                pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.3.1");
                //pdu.VbList.Add("1.3.6.1.2.1.1.1.0");
                SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);
                if (result != null)
                {
                    var temp = new Printer(target, result.Pdu.VbList[0].Value.ToString());

                    if (!Devices.Contains(temp))
                        Devices.Add(temp);

                }
                target.Close();
            }
        }

        /// <summary>
        /// Устанавливает трап на oid устройства
        /// </summary>
        /// <param name="printer"></param>
        public void SetTrap(IpAddress adr, Oid oid)
        {
            // Variable Binding collection to send with the trap
            VbCollection col = new VbCollection();
            col.Add(oid, new OctetString());
            agent.SendV1Trap(adr, 162, "public",
                             new Oid("1.3.6.1.2.1.1.1.0"), new IpAddress("127.0.0.1"),
                             SnmpConstants.LinkUp, 0, 13432, col);
        }

        public void TestGetNext(string ip = "localhost", string mib = "1.3.6.1.2.1.1.1")
        {
            IpAddress ipforscan;

            try
            {
                ipforscan = new IpAddress(ip);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
            OctetString community = new OctetString("public");
            AgentParameters param = new AgentParameters(community);
            param.Version = SnmpVersion.Ver1;
            UdpTarget target = new UdpTarget((System.Net.IPAddress)ipforscan, 161, 20, 1);
            Pdu pdu = new Pdu();
            pdu.Type = PduType.GetNext;
            pdu.RequestId = 11; // Set a custom request id
            pdu.VbList.Add(mib);
            SnmpV1Packet result = null;
            try
            {
                result = (SnmpV1Packet)target.Request(pdu, param);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            if (result != null)
            {
                Console.WriteLine("Result: ");
                foreach (var Vb in result.Pdu.VbList)
                {
                    Console.Write(Vb.Value + " ");
                }
            }
            else
            {
               Console.WriteLine("Unnable to ask this device");
            }
        }

    }
}
