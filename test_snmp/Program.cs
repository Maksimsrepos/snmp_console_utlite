﻿using SnmpSharpNet;
using System;
using System.Net;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Xml;
using System.Xml.Linq;


namespace ConsoleApp1
{
    class Program
    {
        public static ConsoleKeyInfo Exit_pr { get; set; }

        static void Main(string[] args)
        {

            {
                Test();
                Console.ReadKey();
            }
            while (true);

        }

        public static void Test()
        {
            string path = "C:\\Users\\admin\\source\\repos\\test_snmp\\test_snmp\\OIDsConfig.xml";

            var xDoc = new XmlDocument();

            xDoc.Load(path);
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            // обход всех узлов в корневом элементе
            foreach (XmlNode xnode in xRoot)
            {
                // получаем атрибут name
                if (xnode.Attributes.Count > 0)
                {
                    XmlNode attr = xnode.Attributes.GetNamedItem("name");
                    if (attr != null)
                        Console.WriteLine(attr.Value);
                }
                // обходим все дочерние узлы элемента user
                foreach (XmlNode childnode in xnode.ChildNodes)
                {
                   Console.WriteLine(childnode.InnerText);
                }
                Console.WriteLine();
            }



            //ReadXmlFile(path);
        }
        

/// <summary>
/// Прочитать Xml файл.
/// </summary>
/// <param name="filename"> Путь к Xml файлу. </param>
private static void ReadXmlFile(string filename)
        {
            // Создаем экземпляр Xml документа.
            var doc = new XmlDocument();

            // Загружаем данные из файла.
            doc.Load(filename);

            // Получаем корневой элемент документа.
            var root = doc.DocumentElement;

            // Используем метод для рекурсивного обхода документа.
            PrintItem(root);
        }

        /// <summary>
        /// Метод для отображения содержимого xml элемента.
        /// </summary>
        /// <remarks>
        /// Получает элемент xml, отображает его имя, затем все атрибуты
        /// после этого переходит к зависимым элементам.
        /// Отображает зависимые элементы со смещением вправо от начала строки.
        /// </remarks>
        /// <param name="item"> Элемент Xml. </param>
        /// <param name="indent"> Количество отступов от начала строки. </param>
        private static void PrintItem(XmlElement item, int indent = 0)
        {
            // Выводим имя самого элемента.
            // new string('\t', indent) - создает строку состоящую из indent табов.
            // Это нужно для смещения вправо.
            // Пробел справа нужен чтобы атрибуты не прилипали к имени.
            Console.Write($"{new string('\t', indent)}{item.LocalName} ");

            // Если у элемента есть атрибуты, 
            // то выводим их поочередно, каждый в квадратных скобках.
            foreach (XmlAttribute attr in item.Attributes)
            {
                Console.Write($"[{attr.InnerText}]");
            }

            // Если у элемента есть зависимые элементы, то выводим.
            foreach (var child in item.ChildNodes)
            {
                if (child is XmlElement node)
                {
                    // Если зависимый элемент тоже элемент,
                    // то переходим на новую строку 
                    // и рекурсивно вызываем метод.
                    // Следующий элемент будет смещен на один отступ вправо.
                    Console.WriteLine();
                    PrintItem(node, indent + 1);
                }

                if (child is XmlText text)
                {
                    // Если зависимый элемент текст,
                    // то выводим его через тире.
                    Console.Write($"- {text.InnerText}");
                }
            }
        }
    }
}


/*


          Console.WriteLine("Welcome to the SNMP console utility. Console commands:" +

                             "\n a - for get oid info by ip" +
                             "\n s - for scan in range" +
                             "\n g - for get all oids info by ip" +
                             "\n r - rescan all previosly finded devices" +
                             "\n t - set trap"
                             );

          Console.WindowWidth = Console.BufferWidth + 30;
          Console.WindowHeight += 30;

          string command;

          SuperSimpleSnmp snmp = new SuperSimpleSnmp();

          string ipfrom, ipto;

          do
          {
              Console.Write("Snmp:> ");
              command = Console.ReadLine();
              switch (command[0])
              {
                  case ('a'):
                      Console.WriteLine("Input ip: ");
                      var ip = Console.ReadLine();
                      Console.WriteLine("Input mib: ");
                      var mib = Console.ReadLine();
                      snmp.TestGetNext(ip, mib);
                      break;

                  case ('s'):

                      try
                      {
                          Console.WriteLine("Input start ip: ");
                          ipfrom = Console.ReadLine();
                          Console.WriteLine("Input finish ip: ");
                          ipto = Console.ReadLine();
                      }
                      catch (Exception e)
                      {
                          Console.WriteLine(e);
                          throw;
                      }

                      snmp.StartScan(new IpAddress(ipfrom), new IpAddress(ipto));

                      foreach (var i in snmp.Devices)
                          Console.WriteLine($"{i.Ip} : {i.Naim} : {i.Description} : {i.TonerMax} : {i.TonerCurrent} : {i.TonerType} : {i.PrintedPages}");
                      break;

                  case ('g'):

                      Console.WriteLine("Input ip to scan all mibs: ");
                      try
                      {
                          snmp.GetInfo(Console.ReadLine());
                      }
                      catch (Exception e)
                      {
                          Console.WriteLine(e);
                          throw;
                      }

                      break;
                  case ('r'):

                      try
                      {
                          Console.WriteLine("Rescaning...");
                          snmp.Rescan();
                      }
                      catch (Exception e)
                      {
                          Console.WriteLine(e);
                          throw;
                      }

                      break;

                  case ('t'):
                      try
                      {
                          Console.WriteLine("Input device ip: ");
                          var ipt = new IpAddress(Console.ReadLine());

                          Console.WriteLine("Input device ip: ");
                          var mibt = new Oid(Console.ReadLine());

                          snmp.SetTrap(ipt, mibt);
                      }
                      catch (Exception e)
                      {
                          Console.WriteLine(e);
                          throw;
                      }

                      break;

                  default:
                      Test();
                      break;
              }*/
