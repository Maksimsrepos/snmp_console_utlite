﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnmpSharpNet;

namespace ConsoleApp1
{
    public class Printer: Device
    {
        
        public int TonerMax { get; private set; }
        public int TonerCurrent { get; private set; }
        public string TonerType { get; private set; }
        public int PrintedPages { get; private set; }
        public string WhereIs { get; private set; }

        public Printer(UdpTarget target, string naim)
        {

            OctetString community = new OctetString("public");

            AgentParameters param = new AgentParameters(community);

            Pdu pdu = new Pdu(PduType.Get);

           
            if (naim.Contains("ECOSYS"))
            {
                pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.3.1"); //Наименование
                pdu.VbList.Add("1.3.6.1.2.1.1.6.0");//Расположение?
                pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.8.1.1");//Уровень тонера общий (Емкость отсека)
                pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.9.1.1");//Уровень тонера текущий
            }
            else if (naim.Contains("Samsung"))
            {
                pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.3.1"); //Наименование
                pdu.VbList.Add("1.3.6.1.2.1.1.6.0");//Расположение?
                pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.8.1.1");//Уровень тонера общий (Емкость отсека)
                pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.9.1.1");//Уровень тонера текущий
            }
            else if (naim.Contains("Canon"))
            {
                pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.3.1"); //Наименование
                pdu.VbList.Add("1.3.6.1.2.1.1.6.0");//Расположение
                pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.8.1.1");//Уровень тонера общий (Емкость отсека)
                pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.9.1.1");//Уровень тонера текущий
            }
            else
            {
                pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.3.1"); //Наименование
                pdu.VbList.Add("1.3.6.1.2.1.1.6.0");//Расположение
            }


            try
            {

                SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);


                if (result != null)
                {
                    result.Pdu.VbList[0].Value.ToString();
                    this.Ip = new IpAddress(target.Address);
                    this.Naim = result.Pdu.VbList[0].Value.ToString();
                    this.WhereIs = result.Pdu.VbList[1].Value.ToString();
                    if (pdu.VbList.Count > 2)
                    {
                        this.TonerMax = Convert.ToInt32(result.Pdu.VbList[2].Value.ToString());
                        this.TonerCurrent = Convert.ToInt32(result.Pdu.VbList[3].Value.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка при создании принтера " +e.Message);
            }
        }

    }
}
