﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnmpSharpNet;

namespace ConsoleApp1
{
    public class Device
    {
        public string Naim { get; set; }
        public IpAddress Ip { get; set; }
        public string Description { get; private set; }
    }
}
