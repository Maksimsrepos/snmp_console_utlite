﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.BL;
using WebApplication1;
namespace WebApplication1.Controllers
{

    public class HomeController : Controller
    {

        // GET: Home
        public ViewResult Index()
        {


            ViewBag.TestList = (List<PrintedDevice>)MvcApplication.Snmp.Devices;

            return View();
        }

    }
}