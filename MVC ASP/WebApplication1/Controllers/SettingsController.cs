﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class SettingsController : Controller
    {
        
        
        public ActionResult Index()
        {
            List<SettingsModels> lstProject = new List<SettingsModels>();

            DataSet ds = new DataSet();

            var filename = "OIDsConfig.xml";
            var currentDirectory = Directory.GetCurrentDirectory();
            var resFile = Path.Combine(currentDirectory, filename);

            ds.ReadXml(resFile);

            DataView dvPrograms;

            dvPrograms = ds.Tables[0].DefaultView;

            //dvPrograms.Sort = "Id";

            foreach (DataRowView dr in dvPrograms)
            {
                SettingsModels model = new SettingsModels();
                //model.Id = Convert.ToInt32(dr[0]);

                //model.Manufacurer = Convert.ToString(dr[1]);

                //model.Oid = Convert.ToString(dr[2]);
                lstProject.Add(model);
            }
            if (lstProject.Count > 0)
            {
                return View(lstProject);
            }
            return View();

        }

        SettingsModels model = new SettingsModels();

        public ActionResult AddEditProject(int? id)
        {

            int Id = Convert.ToInt32(id);
            if (Id > 0)
            {
                GetDetailsById(Id);
               
                return View(model);
            }
            else
            {
              
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult AddEditProject(SettingsModels mdl)
        {
            string filename = "OIDsConfig.xml";
            string currentDirectory = Directory.GetCurrentDirectory();
            string resFile = Path.Combine(currentDirectory, filename);


            if (mdl.Id > 0)
            {
                XDocument xmlDoc = XDocument.Load(resFile);
                var items = (from item in xmlDoc.Descendants("Manufacurer")
                             select item).ToList();
                XElement selected = items.Where(p => p.Element("Id").Value == mdl.Id.ToString()).FirstOrDefault();


                selected.Remove();
                xmlDoc.Save(resFile);
                xmlDoc.Element("Manufacurers").Add(new XElement("Manufacurer",
                                                                new XElement("Id", mdl.Id),

                                                                new XElement("Oid", mdl.Oid),

                                                                new XElement("Description", mdl.Description)

                                                           )
                                            );
                xmlDoc.Save(Server.MapPath("OidsConfig.xml"));

                return RedirectToAction("Index", "Admin");
            }
            else
            {
                XmlDocument oXmlDocument = new XmlDocument();
                oXmlDocument.Load(resFile);
                XmlNodeList nodelist = oXmlDocument.GetElementsByTagName("Manufacurer");
                var x = oXmlDocument.GetElementsByTagName("Id");
                int Max = 0;
                foreach (XmlElement item in x)
                {
                    int EId = Convert.ToInt32(item.InnerText.ToString());
                    if (EId > Max)
                    {
                        Max = EId;
                    }
                }
                Max = Max + 1;
                XDocument xmlDoc = XDocument.Load(Server.MapPath("OidsConfig.xml"));
                xmlDoc.Element("Manufacurers").Add(new XElement("Manufacurer",
                                                                new XElement("Id", Max),

                                                                new XElement("Oid", mdl.Oid),

                                                                new XElement("Oid", mdl.Description)
                                                           )
                                                           );
                xmlDoc.Save(resFile);
                return RedirectToAction("Index", "Admin");
            }

        }







        public void GetDetailsById(int Id)
        {
            string filename = "OIDsConfig.xml";
            string currentDirectory = Directory.GetCurrentDirectory();
            string resFile = Path.Combine(currentDirectory, filename);


            XDocument oXmlDocument = XDocument.Load(resFile);
            var items = (from item in oXmlDocument.Descendants("Manufacurer")
                         where Convert.ToInt32(item.Element("Id").Value) == Id
                         select new projectItems
                         {
                             Id = Convert.ToInt32(item.Element("Id").Value),

                             Oid = item.Element("Oid").Value,

                             Description = item.Element("Description").Value,

                         }).SingleOrDefault();

            if (items != null)
            {
                model.Id = items.Id;

                model.Oid = items.Oid;

                model.Description = items.Description;
            }
        }

        public class projectItems
        {
            public int Id { get; set; }

            public string Oid { get; set; }

            public string Description { get; set; }
            public projectItems()
            {
            }
        }
    }


}