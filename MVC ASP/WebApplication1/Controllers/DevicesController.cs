﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.BL;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class DevicesController : Controller
    {
        // GET: Devices
        public ActionResult Index()
        {


            #region For test

            var random = new Random();

            MvcApplication.Snmp.Devices.Add(new PrintedDevice() { Naim = "1", Description = "dasd", Location = "ds1ad", Ip = new SnmpSharpNet.IpAddress("10.10.0.1"), TonerCurrent = random.Next(10, 1000), TonerMax = 1000, PrintedPages = random.Next(50000) });
            MvcApplication.Snmp.Devices.Add(new PrintedDevice() { Naim = "2", Description = "dasd1", Location = "d2sad", Ip = new SnmpSharpNet.IpAddress("10.10.0.2"), TonerCurrent = random.Next(10, 1000), TonerMax = 1000, PrintedPages = random.Next(50000) });
            MvcApplication.Snmp.Devices.Add(new PrintedDevice() { Naim = "3", Description = "dasd2", Location = "d3sad", Ip = new SnmpSharpNet.IpAddress("10.10.0.3"), TonerCurrent = random.Next(10, 1000), TonerMax = 1000, PrintedPages = random.Next(50000) });
            MvcApplication.Snmp.Devices.Add(new PrintedDevice() { Naim = "4", Description = "dasd3", Location = "d4sad", Ip = new SnmpSharpNet.IpAddress("10.10.0.4"), TonerCurrent = random.Next(10, 1000), TonerMax = 1000, PrintedPages = random.Next(50000) });
            MvcApplication.Snmp.Devices.Add(new PrintedDevice() { Naim = "5", Description = "dasd4", Location = "d5sad", Ip = new SnmpSharpNet.IpAddress("10.10.0.5"), TonerCurrent = random.Next(10, 1000), TonerMax = 1000, PrintedPages = random.Next(50000) });
            MvcApplication.Snmp.Devices.Add(new PrintedDevice() { Naim = "6", Description = "dasd5", Location = "d2sad", Ip = new SnmpSharpNet.IpAddress("10.10.0.6"), TonerCurrent = random.Next(10, 1000), TonerMax = 1000, PrintedPages = random.Next(50000) });

            #endregion

            var list = MvcApplication.Snmp.Devices;

            ViewBag.TestList = (List<PrintedDevice>)list;

            return View();
        }

        [HttpPost]
        public ActionResult ScanForm(ScanerInfo model)
        {

            try
            {

                if (ModelState.IsValid)
                {
                    MvcApplication.Snmp.StartScan(new SnmpSharpNet.IpAddress(model.Ip1), new SnmpSharpNet.IpAddress(model.Ip1));
                    return Index();
                }
                return RedirectToAction("", model);

            }
            catch(Exception)
            {
                return RedirectToAction("", model);
            }

        }
    }
}