﻿using SnmpSharpNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Web.WebPages;
using System.Xml.Linq;




namespace WebApplication1.BL
{
    public class SuperSimpleSnmp
    {
        private XDocument oidsConfig;
        private XDocument devicesConfig;
        public List<PrintedDevice> Devices { get; private set; }
        public List<Trap> Traps { get; private set; }
        private TrapAgent agent;
        private Thread listener;
        private System.Threading.Timer timerRescan;

        public SuperSimpleSnmp()
        {
            Devices = new List<PrintedDevice>();
            Traps = new List<Trap>();
            agent = new TrapAgent();

            try
            {

                var filename = "OIDsConfig.xml";
                var currentDirectory = Directory.GetCurrentDirectory();
                var resFile = Path.Combine(currentDirectory, filename);

                oidsConfig = XDocument.Load(resFile);

            }
            catch
            {
                XDocument doc =
                     new XDocument(
                         new XDeclaration("1.0", Encoding.UTF8.HeaderName, String.Empty),

                         new XElement("SetOfSettings",

                             new XElement("manufacturer", new XAttribute("name", "Ecosys"),
                                 new XElement("Name", "1.3.6.1.2.1.25.3.2.1.3.1"),
                                 new XElement("Description", "1.3.6.1.2.1.1.6.0"),
                                 new XElement("Location", "1.3.6.1.2.1.1.6.0"),
                                 new XElement("TonerMax", "1.3.6.1.2.1.43.11.1.1.8.1.1"),
                                 new XElement("TonerCurrent", "1.3.6.1.2.1.43.11.1.1.9.1.1"),
                                 new XElement("TonerType", "1.3.6.1.2.1.43.11.1.1.9.1.1"),
                                 new XElement("PrintedPages", "1.3.6.1.2.1.43.11.1.1.9.1.1")
                                 ),

                             new XElement("manufacturer", new XAttribute("name", "Samsung"),
                                 new XElement("Name", "1.3.6.1.2.1.25.3.2.1.3.1"),
                                 new XElement("Description", "1.3.6.1.2.1.1.6.0"),
                                 new XElement("Location", "1.3.6.1.2.1.1.6.0"),
                                 new XElement("TonerMax", "1.3.6.1.2.1.43.11.1.1.8.1.1"),
                                 new XElement("TonerCurrent", "1.3.6.1.2.1.43.11.1.1.9.1.1"),
                                 new XElement("TonerType", "1.3.6.1.2.1.43.11.1.1.9.1.1"),
                                 new XElement("PrintedPages", "1.3.6.1.2.1.43.11.1.1.9.1.1")
                             ),

                             new XElement("manufacturer", new XAttribute("name", "Canon"),
                                 new XElement("Name", "1.3.6.1.2.1.25.3.2.1.3.1"),
                                 new XElement("Description", "1.3.6.1.2.1.1.6.0"),
                                 new XElement("Location", "1.3.6.1.2.1.1.6.0"),
                                 new XElement("TonerMax", "1.3.6.1.2.1.43.11.1.1.8.1.1"),
                                 new XElement("TonerCurrent", "1.3.6.1.2.1.43.11.1.1.9.1.1"),
                                 new XElement("TonerType", "1.3.6.1.2.1.43.11.1.1.9.1.1"),
                                 new XElement("PrintedPages", "1.3.6.1.2.1.43.11.1.1.9.1.1")
                             ),

                             new XElement("manufacturer", new XAttribute("name", "Epson"),
                                 new XElement("Name", "1.3.6.1.2.1.25.3.2.1.3.1"),
                                 new XElement("Description", "1.3.6.1.2.1.1.6.0"),
                                 new XElement("Location", "1.3.6.1.2.1.1.6.0"),
                                 new XElement("TonerMax", "1.3.6.1.2.1.43.11.1.1.8.1.1"),
                                 new XElement("TonerCurrent", "1.3.6.1.2.1.43.11.1.1.9.1.1"),
                                 new XElement("TonerType", "1.3.6.1.2.1.43.11.1.1.9.1.1"),
                                 new XElement("PrintedPages", "1.3.6.1.2.1.43.11.1.1.9.1.1")
                             ),

                             new XElement("manufacturer", new XAttribute("name", "default"),
                                new XElement("Name", "1.3.6.1.2.1.25.3.2.1.3.1"),
                                 new XElement("Description", "1.3.6.1.2.1.1.6.0"),
                                 new XElement("Location", "1.3.6.1.2.1.1.6.0"),
                                 new XElement("TonerMax", "1.3.6.1.2.1.43.11.1.1.8.1.1"),
                                 new XElement("TonerCurrent", "1.3.6.1.2.1.43.11.1.1.9.1.1"),
                                 new XElement("TonerType", "1.3.6.1.2.1.43.11.1.1.9.1.1"),
                                 new XElement("PrintedPages", "1.3.6.1.2.1.43.11.1.1.9.1.1")
                             )

                         )

                     );

                doc.Save("OIDsConfig.xml");
                oidsConfig = doc;
            }

            try
            {

                var filename = "DevicesList.xml";
                var currentDirectory = Directory.GetCurrentDirectory();
                var resFile = Path.Combine(currentDirectory, filename);

                devicesConfig = XDocument.Load(resFile);

            }
            catch
            {
                XDocument doc =
                     new XDocument(
                         new XDeclaration("1.0", Encoding.UTF8.HeaderName, String.Empty),

                         new XElement("Devices", "\n\n"));

                doc.Save("DevicesList.xml");

                devicesConfig = doc;
                
            }

            //this.StartScan(new SnmpSharpNet.IpAddress("10.10.0.1"), new SnmpSharpNet.IpAddress("10.10.0.50"));


            #region For test

            var random = new Random();

            Devices.Add(new PrintedDevice() { Naim = "1", Description = "dasd", Location = "ds1ad", Ip = new SnmpSharpNet.IpAddress("10.10.0.1"), TonerCurrent = random.Next(10, 1000), TonerMax = 1000, PrintedPages = random.Next(50000) });
            Devices.Add(new PrintedDevice() { Naim = "2", Description = "dasd1", Location = "d2sad", Ip = new SnmpSharpNet.IpAddress("10.10.0.2"), TonerCurrent = random.Next(10, 1000), TonerMax = 1000, PrintedPages = random.Next(50000) });
            Devices.Add(new PrintedDevice() { Naim = "3", Description = "dasd2", Location = "d3sad", Ip = new SnmpSharpNet.IpAddress("10.10.0.3"), TonerCurrent = random.Next(10, 1000), TonerMax = 1000, PrintedPages = random.Next(50000) });
            Devices.Add(new PrintedDevice() { Naim = "4", Description = "dasd3", Location = "d4sad", Ip = new SnmpSharpNet.IpAddress("10.10.0.4"), TonerCurrent = random.Next(10, 1000), TonerMax = 1000, PrintedPages = random.Next(50000) });
            Devices.Add(new PrintedDevice() { Naim = "5", Description = "dasd4", Location = "d5sad", Ip = new SnmpSharpNet.IpAddress("10.10.0.5"), TonerCurrent = random.Next(10, 1000), TonerMax = 1000, PrintedPages = random.Next(50000) });
            Devices.Add(new PrintedDevice() { Naim = "6", Description = "dasd5", Location = "d2sad", Ip = new SnmpSharpNet.IpAddress("10.10.0.6"), TonerCurrent = random.Next(10, 1000), TonerMax = 1000, PrintedPages = random.Next(50000) });

            #endregion

            listener = new Thread(() =>
            {
                // Construct a socket and bind it to the trap manager port 162 
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 162);
                EndPoint ep = (EndPoint)ipep;
                socket.Bind(ep);
                // Disable timeout processing. Just block until packet is received 
                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 0);
                bool run = true;
                int inlen = -1;
                while (run)
                {
                    byte[] indata = new byte[16 * 1024];
                    // 16KB receive buffer int inlen = 0;
                    IPEndPoint peer = new IPEndPoint(IPAddress.Any, 0);
                    EndPoint inep = (EndPoint)peer;
                    try
                    {
                        inlen = socket.ReceiveFrom(indata, ref inep);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception {0}", ex.Message);
                        inlen = -1;
                    }
                    if (inlen > 0)
                    {
                        // Check protocol version int 
                        int ver = SnmpPacket.GetProtocolVersion(indata, inlen);
                        if (ver == (int)SnmpVersion.Ver1)
                        {
                            // Parse SNMP Version 1 TRAP packet 
                            SnmpV1TrapPacket pkt = new SnmpV1TrapPacket();
                            pkt.decode(indata, inlen);
                            Console.WriteLine("** SNMP Version 1 TRAP received from {0}:", inep.ToString());
                            Console.WriteLine("*** Trap generic: {0}", pkt.Pdu.Generic);
                            Console.WriteLine("*** Trap specific: {0}", pkt.Pdu.Specific);
                            Console.WriteLine("*** Agent address: {0}", pkt.Pdu.AgentAddress.ToString());
                            Console.WriteLine("*** Timestamp: {0}", pkt.Pdu.TimeStamp.ToString());
                            Console.WriteLine("*** VarBind count: {0}", pkt.Pdu.VbList.Count);
                            Console.WriteLine("*** VarBind content:");
                            foreach (Vb v in pkt.Pdu.VbList)
                            {
                                Console.WriteLine("**** {0} {1}: {2}", v.Oid.ToString(), SnmpConstants.GetTypeName(v.Value.Type), v.Value.ToString());
                            }
                            Console.WriteLine("** End of SNMP Version 1 TRAP data.");
                        }
                        else
                        {
                            // Parse SNMP Version 2 TRAP packet 
                            SnmpV2Packet pkt = new SnmpV2Packet();
                            pkt.decode(indata, inlen);
                            Console.WriteLine("** SNMP Version 2 TRAP received from {0}:", inep.ToString());
                            if ((SnmpSharpNet.PduType)pkt.Pdu.Type != PduType.V2Trap)
                            {
                                Console.WriteLine("*** NOT an SNMPv2 trap ****");
                            }
                            else
                            {
                                Console.WriteLine("*** Community: {0}", pkt.Community.ToString());
                                Console.WriteLine("*** VarBind count: {0}", pkt.Pdu.VbList.Count);
                                Console.WriteLine("*** VarBind content:");
                                foreach (Vb v in pkt.Pdu.VbList)
                                {
                                    Console.WriteLine("**** {0} {1}: {2}",
                                       v.Oid.ToString(), SnmpConstants.GetTypeName(v.Value.Type), v.Value.ToString());
                                }
                                Console.WriteLine("** End of SNMP Version 2 TRAP data.");
                            }
                        }
                    }
                    else
                    {
                        if (inlen == 0)
                            Console.WriteLine("Zero length packet received.");
                    }
                }
            });

            listener.Start();

            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromMinutes(15);

            timerRescan = new System.Threading.Timer((e) =>
            {
                UpdateInfo();
            }, null, startTimeSpan, periodTimeSpan);





        }
        /// <summary>
        /// Выполняет сканирование по диапазону, от начального до конечного IP адреса
        /// </summary>
        /// <param name="fromIp">Начальный адрес</param>
        /// <param name="toIp">Конечный адрес</param>
        public void StartScan(IpAddress fromIp, IpAddress toIp)
        {


            IpAddress ipforscan = fromIp;

            OctetString community = new OctetString("public");

            AgentParameters param = new AgentParameters(community);

            param.Version = SnmpVersion.Ver1;

            Dictionary<IpAddress, string> resultDict = new Dictionary<IpAddress, string>();

            while (ipforscan.CompareTo(toIp) == -1)
            {

                try
                {

                    UdpTarget target = new UdpTarget((System.Net.IPAddress)ipforscan, 161, 2, 2);

                    Pdu pdu = new Pdu(PduType.Get);
                    pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.3.1");

                    SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);

                    if (result != null)
                    {
                        var temp = new PrintedDevice(target, result.Pdu.VbList[0].Value.ToString(), oidsConfig);

                        if (!Devices.Contains(temp) && temp.Ip != null)
                            Devices.Add(temp);

                    }
                    target.Close();
                }
                catch (Exception exs)
                {
                    Console.WriteLine(exs.Message);
                }


                ipforscan = ipforscan.Increment(1);

            }
        }

        /// <summary>
        /// Выполняет сканирование по списку IP адресов
        /// </summary>
        /// <param name="list"></param>
        public void StartScan(List<IpAddress> list)
        {

            OctetString community = new OctetString("public");

            AgentParameters param = new AgentParameters(community);

            param.Version = SnmpVersion.Ver1;

            foreach (IpAddress ipforscan in list)
            {

                try
                {

                    UdpTarget target = new UdpTarget((System.Net.IPAddress)ipforscan, 161, 2, 2);

                    Pdu pdu = new Pdu(PduType.Get);
                    pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.3.1");
                    //pdu.VbList.Add("1.3.6.1.2.1.1.1.0");
                    SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);
                    if (result != null)
                    {
                        var temp = new PrintedDevice(target, result.Pdu.VbList[0].Value.ToString(), oidsConfig);

                        if (!Devices.Contains(temp) && temp.Ip != null)
                            Devices.Add(temp);

                    }
                    target.Close();
                }
                catch (Exception exs)
                {
                    Console.WriteLine(exs.Message);
                }

            }
        }





        /// <summary>
        /// Выполняет актуализацию данных всех принтеров находящихся в коллекции Devices
        /// Актуализация выполняется путём пересоздания объекта устройства.
        /// Создает отчет для ERP системы
        /// </summary>
        public void UpdateInfo()
        {
            var tempcol = new List<PrintedDevice>();
            foreach (var printer in Devices)
            {
                try
                {

                    OctetString community = new OctetString("public");

                    AgentParameters param = new AgentParameters(community);

                    param.Version = SnmpVersion.Ver1;

                    UdpTarget target = new UdpTarget((System.Net.IPAddress)printer.Ip, 161, 2, 2);

                    Pdu pdu = new Pdu(PduType.Get);
                    pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.3.1");
                    SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);
                    if (result != null)
                    {
                        var temp = new PrintedDevice(target, result.Pdu.VbList[0].Value.ToString(), oidsConfig);

                        if (!Devices.Contains(temp) && temp.Ip != null)
                            tempcol.Add(temp);
                    }
                    target.Close();
                }
                catch (Exception exs)
                {
                    Console.WriteLine(exs.Message);
                }

            }

            Devices = new List<PrintedDevice>(tempcol);
            tempcol.Clear();


            try
            {

                var pathForReport = XDocument.Load("~/MainConfig.xml").Element("PathForReport").Value;
                var filename = "Report.xml";
                var resFile = Path.Combine(pathForReport, filename);

                var doc = new XDocument(resFile);


                foreach (var device in Devices)
                {
                    if (device.TonerCurrent <= 20)
                    {
                        doc.Root.Add(new XElement("device", $"{device.Naim} знаходиться в {device.Location} "));
                    }
                }


                doc.Save(resFile);

            }
            catch
            {
                XDocument doc =
                    new XDocument(
                        new XDeclaration("1.0", Encoding.UTF8.HeaderName, String.Empty),

                        new XElement("Devices", "\n\n"));


                foreach (var device in Devices)
                {
                    if (device.TonerCurrent <= 20)
                    {
                        doc.Root.Add(new XElement("device", $"{device.Naim} знаходиться в {device.Location}, тонер типу {device.TonerType}"));
                    }
                }


                var pathForReport = XDocument.Load("~/MainConfig.xml").Element("PathForReports").Value;


                doc.Save(Path.Combine(pathForReport, "/Report.xml"));

            }






        }





        /// <summary>
        /// Устанавливает трап на oid устройства.
        /// </summary>
        /// <param name="adr"></param>
        /// <param name="oid"></param>
        /// <param name="desc"></param>
        public void SetTrap(IpAddress adr, Oid oid, string desc)
        {
            // Variable Binding collection to send with the trap
            VbCollection col = new VbCollection();
            col.Add(oid, new OctetString());
            agent.SendV1Trap(adr, 162, "public",
                             oid, adr,
                             SnmpConstants.LinkUp, 0, 13432, col);

            var temp = new Trap() { OID = oid, Description = desc, IP = adr };

            if (!Traps.Contains(temp))
                Traps.Add(temp);
            else
                Traps.Remove(temp);
        }

        /// <summary>
        /// Добавляет устройство в список
        /// </summary>
        /// <param name="adr"></param>
        public void AddDevice(string adr)
        {
            devicesConfig.Root.Add(
                new XElement("Device", adr)
                );
            devicesConfig.Save("DevicesList.xml");

        }
         /// <summary>
         /// Удаляет устройство из списка
         /// </summary>
         /// <param name="adr"></param>
        public void DeleteDevice(string adr)
        {
            devicesConfig.Descendants().Where(e => e.Value == adr).Remove();
            devicesConfig.Save("DevicesList.xml");

        }
        
        /// <summary>
        /// Добавлет параметр для опроса
        /// </summary>
        /// <param name="manufacturer"></param>
        /// <param name="oid"></param>
        /// <param name="description"></param>
        public void AddOID(string manufacturer, string oid, string description)
        {
            oidsConfig.Element("SetOfSettings").Element(manufacturer).Add(new XElement(description, oid));
            oidsConfig.Save("OIDsConfig.xml");
        }

        /// <summary>
        /// Удаляет параметр для опроса
        /// </summary>
        /// <param name="manufacturer"></param>
        /// <param name="oid"></param>
        /// <param name="description"></param>
        public void DeleteOID(string manufacturer, string oid, string description)
        {
            oidsConfig.Element("SetOfSettings").Element(manufacturer).Remove();
            oidsConfig.Save("OIDsConfig.xml");
        }

        /// <summary>
        /// Вносит измененик в заданный параметр опроса, по названию параметра.
        /// </summary>
        /// <param name="manufacturer"></param>
        /// <param name="oid"></param>
        /// <param name="description"></param>
        public void ChangeOID(string manufacturer, string oid, string description)
        {
            var xElement = oidsConfig
                .Element("Manufacturers")
                .Elements(manufacturer)
                .Where(e => !e.Element(description).Value.IsEmpty())
                .Single();

            xElement.Element(description).Value = oid;
            oidsConfig.Save("OIDsConfig.xml");
        }

    }
}
