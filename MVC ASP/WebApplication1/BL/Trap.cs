﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using SnmpSharpNet;

namespace WebApplication1.BL
{
    public class Trap
    {
        public IpAddress IP { get; set; }

        public Oid OID { get; set; }

        public string Description { get; set; }
    }
}