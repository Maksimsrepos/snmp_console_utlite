﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnmpSharpNet;

namespace WebApplication1.BL
{
    public class Device
    {
        public string Naim { get; set; }
        public IpAddress Ip { get; set; }
        public string Description { get;  set; }
    }
}
