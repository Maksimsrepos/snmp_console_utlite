﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SnmpSharpNet;

namespace WebApplication1.BL
{
    public class PrintedDevice : Device
    {
        public string Location { get; set; }
        public int TonerMax { get; set; }
        public int TonerCurrent { get; set; }
        public int TonerLvl
        {

            get
            {

                try
                {
                    return (TonerCurrent / (TonerMax / 100));
                }
                catch (DivideByZeroException)
                {
                    return 0;
                }
            }

        }
        public string TonerType { get; set; }
        public int PrintedPages { get; set; }

        public PrintedDevice(UdpTarget target, string naim, XDocument settings)
        {

            OctetString community = new OctetString("public");

            AgentParameters param = new AgentParameters(community);

            Pdu pdu = new Pdu(PduType.Get);




            pdu.VbList.Add(settings.Element("Manufacturers").Elements(naim).Where(e => !e.Element("Name").IsEmpty).Single().Value); //Наименование
            pdu.VbList.Add(settings.Element("Manufacturers").Elements(naim).Where(e => !e.Element("Description").IsEmpty).Single().Value); //Уровень тонера текущий
            pdu.VbList.Add(settings.Element("Manufacturers").Elements(naim).Where(e => !e.Element("Location").IsEmpty).Single().Value); //Расположение
            pdu.VbList.Add(settings.Element("Manufacturers").Elements(naim).Where(e => !e.Element("TonerMax").IsEmpty).Single().Value); //Уровень тонера общий
            pdu.VbList.Add(settings.Element("Manufacturers").Elements(naim).Where(e => !e.Element("TonerCurrent").IsEmpty).Single().Value); //Уровень тонера текущий
            pdu.VbList.Add(settings.Element("Manufacturers").Elements(naim).Where(e => !e.Element("TonerType").IsEmpty).Single().Value); //Тип катриджа
            pdu.VbList.Add(settings.Element("Manufacturers").Elements(naim).Where(e => !e.Element("PrintedPages").IsEmpty).Single().Value); //Страниц отпечатано


            try
            {

                SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);


                if (result != null)
                {
                    result.Pdu.VbList[0].Value.ToString();
                    this.Ip = new IpAddress(target.Address);
                    this.Naim = result.Pdu.VbList[0].Value.ToString();
                    this.Description = result.Pdu.VbList[1].Value.ToString();
                    this.Location = result.Pdu.VbList[2].Value.ToString();
                    this.TonerMax = Convert.ToInt32(result.Pdu.VbList[3].Value.ToString());
                    this.TonerCurrent = Convert.ToInt32(result.Pdu.VbList[4].Value.ToString());
                    this.PrintedPages = Convert.ToInt32(result.Pdu.VbList[5].Value.ToString());

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка при создании принтера " + e.Message);
            }
        }

        public PrintedDevice()
        {
        }
    }
}
