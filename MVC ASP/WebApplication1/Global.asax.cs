﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebApplication1.BL;

namespace WebApplication1
{
    
    public class MvcApplication : System.Web.HttpApplication
    {
        public static SuperSimpleSnmp Snmp { get; private set; }

       

        protected void Application_Start()
        {

            Snmp = new SuperSimpleSnmp();

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
