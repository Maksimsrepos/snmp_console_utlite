﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{

    public class SettingsModels
    {
        
        public int Id { get; set; }

        [Required]
        public string Manufacurer { get; set; }

        [Required]
        public string Oid { get; set; }

        [Required]
        public string Description { get; set; }
    }


}